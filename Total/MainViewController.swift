//
//  MainViewController.swift
//  Total
//
//  Created by bodyfriend_dev on 2017. 7. 4..
//  Copyright © 2017년 bodyfriend_dev. All rights reserved.
//

import UIKit
import WebKit

class MainViewController: UIViewController, WKNavigationDelegate, WKUIDelegate, WKScriptMessageHandler {
    
    @IBOutlet weak var mainContentV: UIView!
    @IBOutlet weak var loadingV: UIView!
    
    var mainWV: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initLayout()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        mainWV.frame = mainContentV.bounds
    }

    func initLayout() {
        loadWebPage(Constants.URL.BASE)
    }
    
    func loadWebPage(_ url: String) {
        let userContentController = WKUserContentController()
        if let cookies = HTTPCookieStorage.shared.cookies {
            let script = getJSCookiesString(for: cookies)
            let cookieScript = WKUserScript(source: script, injectionTime: .atDocumentStart, forMainFrameOnly: false)
            userContentController.addUserScript(cookieScript)
        }
        let webViewConfig = WKWebViewConfiguration()
        webViewConfig.userContentController = userContentController
        webViewConfig.userContentController.add(self, name: "notificationBridge")

        mainWV = WKWebView(frame: mainContentV.bounds, configuration: webViewConfig)
        mainWV.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mainWV.backgroundColor = UIColor.white
        mainContentV.addSubview(mainWV)
        mainWV.uiDelegate = self
        mainWV.navigationDelegate = self
        
        let myurl = URL(string:url)
        var request = URLRequest(url: myurl!)
        request.httpShouldHandleCookies = true
        mainWV.load(request)
    }

    func getJSCookiesString(for cookies: [HTTPCookie]) -> String {
        var result = ""
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "EEE, d MMM yyyy HH:mm:ss zzz"

        for cookie in cookies {
            result += "document.cookie='\(cookie.name)=\(cookie.value); domain=\(cookie.domain); path=\(cookie.path); "
            if let date = cookie.expiresDate {
                result += "expires=\(dateFormatter.string(from: date)); "
            }
            if (cookie.isSecure) {
                result += "secure; "
            }
            result += "'; "
        }
        return result
    }
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        print("didCommit :", webView.url?.absoluteString as Any)
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        loadingV.isHidden = false
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        loadingForDelay()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        loadingForDelay()
    }
    
    func webView(_ webView: WKWebView, commitPreviewingViewController previewingViewController: UIViewController) {
        print("commitPreviewingViewController")
    }
    
    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
        print("didReceiveServerRedirectForProvisionalNavigation")
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print("didFailProvisionalNavigation")
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        let url = navigationAction.request.url!
        print("decidePolicyFor :", url)
        if url.absoluteString.contains("promotionNotice.view") {
            decisionHandler(.cancel)
        } else if url.absoluteString.contains("cake.view") {
            openLink("http://www.cafedebodyfriend.co.kr/menu.html")
            decisionHandler(.cancel)
        } else if url.scheme != "http" && url.scheme != "https" {
            openLink(url.absoluteString)
            decisionHandler(.cancel)
        } else {
            decisionHandler(.allow)
        }
    }
    
    /// 알럿 컨트롤
    ///
    /// - Parameters:
    ///   - webView: webView
    ///   - message: message
    ///   - frame: frame
    ///   - completionHandler: completionHandler
    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping () -> Void) {
        // 메시지창 컨트롤러 인스턴스를 생성한다
        CommonUtil.showAlert(title: "알림", msg: message, target: self, cancel: nil) {
            completionHandler()
        }
    }
    
    /// 자바스크립트 컨펌 컨트롤
    ///
    /// - Parameters:
    ///   - webView: webView
    ///   - message: message
    ///   - frame: frame
    ///   - completionHandler: completionHandler
    func webView(_ webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (Bool) -> Void) {
        CommonUtil.showAlert(title: "알림", msg: message, target: self, cancel: {
            completionHandler(false)
        }) {
            completionHandler(true)
        }
    }
    
    func webView(_ webView: WKWebView, runJavaScriptTextInputPanelWithPrompt prompt: String, defaultText: String?, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (String?) -> Void) {
        print("runJavaScriptTextInputPanelWithPrompt")
        completionHandler(defaultText)
    }
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        print("didReceive message :", message)
    }
    
    func openLink(_ string: String) {
        let url = URL(string: string)!
        if UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    func loadingForDelay() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loadingV.isHidden = true
        }
    }
    
    // Helper function inserted by Swift 4.2 migrator.
    fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
        return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
    }
}

