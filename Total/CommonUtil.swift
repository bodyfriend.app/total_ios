//
//  CommonUtil.swift
//  service
//
//  Created by insung on 2018. 7. 10..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

class CommonUtil {
    class func showAlert(msg: String, target: UIViewController, cancel: (() -> Void)?, confirm: (() -> Void)?) {
        self.showAlert(title: "", msg: msg, target: target, cancel: cancel, confirm: confirm)
    }
    
    class func showAlert(title: String, msg: String, target: UIViewController, cancel: (() -> Void)?, confirm: (() -> Void)?) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        if (cancel != nil) {
            let cancelAction = UIAlertAction(title: "취소", style: .cancel) { (_) in
                cancel?()
            }
            alert.addAction(cancelAction)
        }
        
        let confirmAction = UIAlertAction(title: "확인", style: .default) { (_) in
            confirm?()
        }
        alert.addAction(confirmAction)
        
        DispatchQueue.main.async {
            target.present(alert, animated: true, completion: nil)
        }
    }
    
    class func openAppSetting() {
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        DispatchQueue.main.async {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(settingsUrl, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            } else {
                UIApplication.shared.openURL(settingsUrl)
            }
        }
    }
    
    class func reloadMainWebView(_ link: String) {
        if let mainVC = Constants.Define.APPDELEGATE.window?.rootViewController as? MainViewController {
            mainVC.mainWV.load(URLRequest(url: URL(string: link)!))
        }
    }
    
    class func showPushAlertData(_ userInfo: [AnyHashable : Any]) {
        if let mainVC = Constants.Define.APPDELEGATE.window?.rootViewController as? MainViewController {
            if let aps = userInfo["aps"] as? [String: Any], let alert = aps["alert"] as? [String: String] {
                if let title = alert["title"], let body = alert["body"] {
                    if let link = userInfo["link"] as? String {
                        CommonUtil.showAlert(title: title, msg: body, target: mainVC, cancel: {
                        }) {
                            let url = Constants.URL.BASE + link
                            CommonUtil.reloadMainWebView(url)
                        }
                    } else {
                        CommonUtil.showAlert(title: title, msg: body, target: mainVC, cancel: nil, confirm: nil)
                    }
                }
            }
        } else {
            Constants.Define.APPDELEGATE.window?.rootViewController?.dismiss(animated: false, completion: {
                showPushAlertData(userInfo)
            })
        }
    }
    
    class func reloadPushData(_ userInfo: [AnyHashable : Any]) {
        if let link = userInfo["link"] as? String {
            let url = Constants.URL.BASE + link
            CommonUtil.reloadMainWebView(url)
        }
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
