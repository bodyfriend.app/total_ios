//
//  Constants.swift
//  customer
//
//  Created by insung on 2018. 7. 3..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

struct Constants {
    struct URL {
        #if DEBUG
//        static let BASE = "http://121.138.34.30:8080/"
//        static let BASE = "http://172.30.40.35:8080/"           // 최대리님
//        static let BASE = "http://172.30.40.37:8081/"           // 재영대리님
        static let BASE = "http://172.30.40.53:8080/m/"           // 종훈
        #else
        static let BASE = "http://t.bodyfriend.co.kr/m/"
        #endif
//        static let BASE = "http://t.bodyfriend.co.kr/m/Test"
    }
    struct Define {
        static let APPDELEGATE = UIApplication.shared.delegate as! AppDelegate
    }
    
    struct Identifier {
        static let STORYBOARD_MAIN_NAME = "Main"
        static let STORYBOARD_MAIN = "MainSID"
    }
}

